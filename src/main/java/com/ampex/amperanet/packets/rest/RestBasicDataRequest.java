package com.ampex.amperanet.packets.rest;

import com.ampex.amperabase.IConnectionManager;
import com.ampex.amperabase.IKiAPI;
import com.ampex.amperabase.InvalidAmpBuildException;
import com.ampex.amperanet.packets.PacketGlobal;

public class RestBasicDataRequest implements IRestPacket {
    @Override
    public void process(IKiAPI ki, IConnectionManager connMan, PacketGlobal pg) {
        RestBasicData rbd = new RestBasicData();
        rbd.height = ki.getChainMan().currentHeight().get();
        connMan.sendPacket(rbd);
    }

    @Override
    public void build(byte[] bytes) throws InvalidAmpBuildException {

    }

    @Override
    public byte[] serializeToBytes() {
        return new byte[0];
    }
}
