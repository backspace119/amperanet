package com.ampex.amperanet.packets.rest.pool;

import amp.headless_amplet.HeadlessAmpletWritable;
import amp.headless_amplet.IHeadlessAmpletReadable;
import amp.headless_amplet.IHeadlessAmpletWritable;
import amp.headless_prefixing_strategies.IHeadlessPrefixingStrategyReadable;
import amp.headless_prefixing_strategies.IHeadlessPrefixingStrategyWritable;
import amp.headless_prefixing_strategies.strategies.complex_hps.ComplexHPSReadable;
import amp.headless_prefixing_strategies.strategies.complex_hps.ComplexHPSWritable;
import com.ampex.amperabase.IConnectionManager;
import com.ampex.amperabase.IKiAPI;
import com.ampex.amperabase.InvalidAmpBuildException;
import com.ampex.amperanet.packets.PacketGlobal;

import java.math.BigInteger;

public class RestPoolData implements IPoolRestPacket {

    public long totalHashrate;
    public BigInteger currentPPS;
    public double dynamicFee;
    public int numberofMiners;
    public long msToNextPayout;
    public long lastTimeFound;
    public BigInteger lastHeightFound;

    @Override
    public void process(IKiAPI ki, IConnectionManager connMan, PacketGlobal pg) {

    }

    @Override
    public void build(byte[] bytes) throws InvalidAmpBuildException {
        IHeadlessPrefixingStrategyReadable hpa = ComplexHPSReadable.create(bytes);
        currentPPS = new BigInteger(hpa.getNextElement().getData());
        lastHeightFound = new BigInteger(hpa.getNextElement().getData());

        IHeadlessAmpletReadable ha = hpa.getNextElement().getDataAsHeadlessAmpletTwinReadable();
        totalHashrate = ha.getNextLong();
        dynamicFee = ha.getNextDouble();
        numberofMiners = ha.getNextInt();
        msToNextPayout = ha.getNextLong();
        lastTimeFound = ha.getNextLong();
    }

    @Override
    public byte[] serializeToBytes() {
        IHeadlessAmpletWritable ha = HeadlessAmpletWritable.create();
        ha.addElement(totalHashrate);
        ha.addElement(dynamicFee);
        ha.addElement(numberofMiners);
        ha.addElement(msToNextPayout);
        ha.addElement(lastTimeFound);
        IHeadlessPrefixingStrategyWritable hpa = ComplexHPSWritable.create();
        hpa.addElement(currentPPS);
        hpa.addElement(lastHeightFound);
        hpa.addElement(ha);
        return hpa.serializeToBytes();
    }
}
