package com.ampex.amperanet.packets.rest.pool;

import com.ampex.adapter.KiAdapter;
import com.ampex.amperabase.IConnectionManager;
import com.ampex.amperabase.IKiAPI;
import com.ampex.amperabase.InvalidAmpBuildException;
import com.ampex.amperabase.StringSettings;
import com.ampex.amperanet.packets.PacketGlobal;

import java.math.BigInteger;

public class RestPoolDataRequest implements IPoolRestPacket {
    @Override
    public void process(IKiAPI ki, IConnectionManager connMan, PacketGlobal pg) {
        RestPoolData rpd = new RestPoolData();
        ki.debug("Attempting to prepare RPD");
        if(ki instanceof  KiAdapter) {
            ki.debug("Preparing RPD to send to REST adatper");
            KiAdapter kiAdapt = ((KiAdapter) ki);
            rpd.currentPPS = BigInteger.valueOf(kiAdapt.getPoolManager().getCurrentPayPerShare().get());
            rpd.numberofMiners = ki.getPoolNet().getConnections().size();
            long totalHR = 0;
            for (String ID : ki.getPoolData().hrMap.keySet()) {
                totalHR += (ki.getPoolData().hrMap.get(ID) / 1000000);
            }
            rpd.totalHashrate = totalHR;
            rpd.dynamicFee = Double.parseDouble(ki.getStringSetting(StringSettings.POOL_FEE));
            long fullTime = Long.parseLong(ki.getStringSetting(StringSettings.POOL_PAYOUT_TIME));
            fullTime *= 60_000;
            rpd.msToNextPayout = fullTime - (System.currentTimeMillis() - kiAdapt.getPoolManager().getCurrentTimeRange().getStartTime());

            rpd.lastTimeFound = ki.getPoolData().timeLastFound;
            if(ki.getPoolData().heightLastFound != null)
            rpd.lastHeightFound = ki.getPoolData().heightLastFound;
            else
                rpd.lastHeightFound = BigInteger.valueOf(-1);
            connMan.sendPacket(rpd);
            ki.debug("Sent RPD 2.0 to REST adapter");
        }
    }

    @Override
    public void build(byte[] bytes) throws InvalidAmpBuildException {

    }

    @Override
    public byte[] serializeToBytes() {
        return new byte[0];
    }
}
