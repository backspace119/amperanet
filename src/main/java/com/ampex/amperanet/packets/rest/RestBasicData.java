package com.ampex.amperanet.packets.rest;

import com.ampex.amperabase.IConnectionManager;
import com.ampex.amperabase.IKiAPI;
import com.ampex.amperabase.InvalidAmpBuildException;
import com.ampex.amperanet.packets.PacketGlobal;

import java.math.BigInteger;

public class RestBasicData implements IRestPacket {

    public BigInteger height;

    @Override
    public void process(IKiAPI ki, IConnectionManager connMan, PacketGlobal pg) {

    }

    @Override
    public void build(byte[] bytes) throws InvalidAmpBuildException {
        height = new BigInteger(bytes);
    }

    @Override
    public byte[] serializeToBytes() {
        return height.toByteArray();
    }
}
