package com.ampex.amperanet.packets;

import amp.HeadlessAmplet;
import com.ampex.amperabase.IConnectionManager;
import com.ampex.amperabase.IKiAPI;
import com.ampex.amperabase.InvalidAmpBuildException;

public class Ping implements Packet {

    public long currentTime;
    @Override
    public void process(IKiAPI ki, IConnectionManager connMan, PacketGlobal pg) {
        Pong pong = new Pong();
        pong.latency = currentTime;
        connMan.sendPacket(pong);
    }

    @Override
    public void build(byte[] serialized) throws InvalidAmpBuildException {
        try {
            HeadlessAmplet ha = HeadlessAmplet.create(serialized);
            currentTime = ha.getNextLong();
        } catch (Exception e) {
            throw new InvalidAmpBuildException("Unable to create Ping from bytes");
        }
    }

    @Override
    public byte[] serializeToBytes() {
        HeadlessAmplet ha = HeadlessAmplet.create();
        ha.addElement(currentTime);
        return ha.serializeToBytes();
    }
}
