package com.ampex.amperanet.packets;

import com.ampex.amperabase.IConnectionManager;
import com.ampex.amperabase.IKiAPI;
import com.ampex.amperabase.InvalidAmpBuildException;

public class TransactionDataRequest implements Packet {


    @Override
    public void process(IKiAPI ki, IConnectionManager connMan, PacketGlobal pg) {
        connMan.sendPacket(new UTXODataStart());

    }

    @Override
    public void build(byte[] serialized) throws InvalidAmpBuildException {

    }

    @Override
    public byte[] serializeToBytes() {
        return new byte[0];
    }
}
