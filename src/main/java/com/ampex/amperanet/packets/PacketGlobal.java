package com.ampex.amperanet.packets;

import com.ampex.amperabase.*;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PacketGlobal implements IPacketGlobal{


    public PacketGlobal(IKiAPI ki, IConnectionManager connMan) {
        this.connMan = connMan;
        this.ki = ki;
    }

    //List<Block> addedBlocks = new ArrayList<>();
    public boolean doneDownloading = false;
    BigInteger startHeight;
    //boolean laFlag = false;
    //boolean onRightChain = true;
    Map<BlockHeader, List<ITransAPI>> bMap = new HashMap<>();
    //Map<BlockHeader, Set<ITrans>> cuMap = new HashMap<>();
    //List<Block> cuBlocks = new ArrayList<>();
    //List<Block> futureBlocks = new ArrayList<>();
    Map<String, BlockHeader> headerMap = new HashMap<>();
    IConnectionManager connMan;
    IKiAPI ki;
    //boolean gotPending = false;
    //boolean cuFlag = false;
    List<String> relays;
    private boolean sfhLock = false;
    void sendFromHeight(BigInteger height) {
        if(sfhLock) return;
        sfhLock = true;

        for (; height.compareTo(ki.getChainMan().currentHeight().get()) <= 0; height = height.add(BigInteger.ONE)) {
            if (height.compareTo(BigInteger.valueOf(-1L)) != 0)
                sendBlock(height);
        }
        if (ki.getChainMan().getTemp() != null && ki.getChainMan().getTemp().getHeight().compareTo(ki.getChainMan().currentHeight().get()) > 0) {
            sendBlock(ki.getChainMan().getTemp());
        }
        sfhLock = false;
    }

    public void cancelAllResends() {
        for (Map.Entry<BigInteger, Thread> height : resendMap.entrySet()) {
            height.getValue().interrupt();
        }
        resendMap.clear();
    }

    @Override
    public boolean doneDownloading() {
        return doneDownloading;
    }

    void sendBlock(final IBlockAPI b) {
        BlockHeader bh2 = formHeader(b);
        connMan.sendPacket(bh2);


        for (String key : b.getTransactionKeys()) {
            TransactionPacket tp = new TransactionPacket();
            tp.block = b.getID();
            tp.trans = b.getTransaction(key).serializeToAmplet().serializeToBytes();
            connMan.sendPacket(tp);
        }
        BlockEnd be = new BlockEnd();
        be.ID = b.getID();
        connMan.sendPacket(be);
        /*
        if (b.height.compareTo(ki.getChainMan().currentHeight()) == 0) {
            for (ITrans trans : ki.getTransMan().getPending()) {
                TransactionPacket tp = new TransactionPacket();
                tp.trans = trans.toJSON();
                connMan.sendPacket(tp);
            }
        }
        */

        /*
        if (!resendMap.containsKey(b.height)) {
            resendTimesMap.merge(b.height, 1, (a, b1) -> a + b1);
            if (resendTimesMap.get(b.height) > 2) {
                ki.debug("Disconnecting connection since retry to send a single block has taken more than 2 tries");
                //connMan.disconnect();
                return;
            }
            */

            /*
            Thread t = new Thread(() -> {
                try {
                    b.height.wait(5000);
                } catch (Exception e) {
                    //fail silently as we expect this to happen
                    //if (ki.getOptions().pDebug)
                        //ki.debug("Block resend #" + b.height.toString() + " interrupted");
                    return;
                }
                ki.debug("Did not receive BlockAck within 5 seconds, resending");
                sendBlock(b);
            });
            t.setName("Block #" + b.height.toString() + " Resend Thread");
            t.start();
            if (lastCancelled.compareTo(b.height) >= 0)
                t.interrupt();
            else
                resendMap.put(b.height, t);
                */

    }
    void sendBlock(final BigInteger height) {
        IBlockAPI b = ki.getChainMan().getByHeight(height);
        sendBlock(b);
    }
    public boolean passiveConnection = false;

    public boolean passiveConnection()
    {
        return passiveConnection;
    }

    //private BigInteger lastCancelled = BigInteger.ZERO;
    //private Map<BigInteger,Integer> resendTimesMap = new HashMap<>();
    private Map<BigInteger,Thread> resendMap = new HashMap<>();
    public void cancelResend(BigInteger height)
    {
        /*
        if(ki.getOptions().pDebug)
        ki.debug("Cancelling resend #" + height.toString());
        if(resendMap.get(height) != null)
        resendMap.get(height).interrupt();

        resendMap.remove(height);
        resendTimesMap.remove(height);
        */
    }

    IBlockAPI formBlock(BlockHeader bh) {
        if (bh == null) {
            ki.debug("We don't have the block header for this block end, our connection to the network must be fucked");
            return null;
        }
        if (bh.prevID == null) {
            ki.debug("Malformed block header received. PrevID is null");
            return null;
        }
        if (bh.ID == null) {
            ki.debug("Malformed block header received. ID is null");
            return null;
        }
        if (bh.height == null) {
            ki.debug("Malformed block header received. height is null");
            return null;
        }
        if (bh.coinbase == null) {
            ki.debug("Malformed block header received. coinbase is null");
            return null;
        }
        if (bh.merkleRoot == null) {
            ki.debug("Malformed block header received. merkleroot is null");
            return null;
        }
        if (bh.payload == null) {
            ki.debug("Malformed block header received. payload is null");
            return null;
        }
        if (bh.solver == null) {
            ki.debug("Malformed block header received. solver is null");
            return null;
        }
        if (bh.timestamp == 0) {
            ki.debug("Malformed block header received. timestamp is impossible");
            return null;
        }
        return ki.getChainMan().formBlock(bh.height,bh.ID,bh.merkleRoot,bh.payload,bh.prevID,bh.solver,bh.timestamp,bh.coinbase);
    }

    BlockHeader formHeader(IBlockAPI b) {
        BlockHeader bh = new BlockHeader();
        bh.timestamp = b.getTimestamp();
        bh.solver = b.getSolver();
        bh.prevID = b.getPrevID();
        bh.payload = b.getPayload();
        bh.merkleRoot = b.merkleRoot();
        bh.ID = b.getID();
        bh.height = b.getHeight();
        bh.coinbase = b.getCoinbase().serializeToAmplet().serializeToBytes();
        return bh;
    }


}
