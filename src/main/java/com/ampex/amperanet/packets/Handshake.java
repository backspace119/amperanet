package com.ampex.amperanet.packets;

import amp.HeadlessAmplet;
import amp.HeadlessPrefixedAmplet;
import com.ampex.amperabase.*;

import java.math.BigInteger;
import java.nio.charset.Charset;
import java.util.ArrayList;

/**
 * Created by Bryan on 7/25/2017.
 */
public class Handshake implements Packet {

    public static final String VERSION = INetworkManagerAPI.NET_VER;
    public static boolean usPassive = false;
    public String ID;
    public String version;
    public BigInteger currentHeight;
    public String mostRecentBlock;
    public short chainVer;
    public boolean isRelay;
    public long startTime;
    public boolean passive = false;
    @Override
    public void process(IKiAPI ki, IConnectionManager connMan, PacketGlobal pg) {

        if (ID == null) return;
        if (currentHeight == null) return;
        if (version == null) return;
        //pg.startHeight = currentHeight;
        if(!passive && !usPassive)
        if (chainVer != ki.getChainMan().getChainVer()) {

            ki.debug("Mismatched chain versions, disconnecting");
            connMan.disconnect();
            return;
        }
        if (!version.equals(Handshake.VERSION)) {
            ki.debug("Mismatched network versions, disconnecting");
            connMan.disconnect();
            return;
        }
        if(!usPassive)
        if (ID.equals(EncryptUtils.sha224(ki.getEncryptMan().getPublicKeyString(ki.getAddMan().getMainAdd().getKeyType()) + startTime))) {
            ki.debug("Connected to ourself, disconnecting");
            connMan.disconnect();
            return;
        }
        connMan.gotHS();

        /* TODO no way to separate this from netty code, need to find a way to wrap it in the future
        if (connMan.getChannel() != null && connMan.getChannel().remoteAddress() != null)
            ki.debug("Address: " + connMan.getAddress());
        else {
            //SOMETHING IS SERIOUSLY FUCKED UP
            connMan.disconnect();
            return;
        }
        */

        connMan.setID(ID);
        if (!ki.getNetMan().connectionInit(ID, connMan)) {
            //TODO uncommented next line to test against possible attacks, will update this to require some work to prove connect is legit later
            //connMan.disconnect();
            ki.debug("Already connected to this address");
            return;
        }
        pg.passiveConnection = passive;
        ki.debug("Received handshake: ");
        ki.debug("ID: " + ID);
        //ki.debug("Most recent block: " + mostRecentBlock);
        ki.debug("version: " + version);
        ki.debug("Height: " + currentHeight);
        ki.debug("Chain ver: " + chainVer);
        if (!ki.getOptions().relay)
            ki.debug("Is Relay: " + isRelay);
        if(!passive && !usPassive) {
//            if (!ki.getOptions().lite && ki.getGUIHook() != null)
//                ki.getGUIHook().setStart(currentHeight);
            if (!ki.getOptions().lite)
                ki.setStartHeight(currentHeight);
        }
        connMan.setStartTime(startTime);

        if(!passive && !usPassive) {
            if (ki.getChainMan().currentHeight().get().compareTo(BigInteger.valueOf(-1L)) == 0)
                connMan.sendPacket(new DoneDownloading());
            if (ki.getOptions().lite) {
                TransactionDataRequest tdr = new TransactionDataRequest();
                connMan.sendPacket(tdr);
                DifficultyRequest dr = new DifficultyRequest();
                connMan.sendPacket(dr);
            }
            ki.getNetMan().sendOrders(connMan);
        }
        if(usPassive)
        {
            TransactionDataRequest tdr = new TransactionDataRequest();
            connMan.sendPacket(tdr);
            pg.doneDownloading = true;
        }

        if (isRelay && !usPassive) {
            if (pg.relays == null) pg.relays = new ArrayList<>();
            pg.relays.add(connMan.getAddress().split(":")[0].replace("/", ""));
            ki.getNetMan().addRelays(pg.relays);

        }

        if(!passive && !usPassive) {
            RelayList rl = new RelayList();
            rl.relays = pg.relays;
            if (rl.relays == null) rl.relays = new ArrayList<>();
            rl.relays.addAll(ki.getNetMan().getRelays());
            connMan.sendPacket(rl);
            if (ki.getNetMan().getConnections().size() > 10 && ki.getNetMan().isRelay()) {
                DisconnectRequest dr = new DisconnectRequest();
                //connMan.sendPacket(dr);
            }

        }
        pg.startHeight = currentHeight;
        if(!passive && !usPassive) {
            if (ki.getChainMan().currentHeight().get().compareTo(BigInteger.valueOf(-1L)) != 0)
                if (currentHeight.compareTo(ki.getChainMan().currentHeight().get()) == 0) {
                    for (ITransAPI trans : ki.getTransMan().getPending()) {
                        TransactionPacket tp = new TransactionPacket();
                        tp.trans = trans.serializeToAmplet().serializeToBytes();
                        connMan.sendPacket(tp);
                    }
                    pg.doneDownloading = true;

                    if (ki.getOptions().pDebug)
                        ki.debug("Relay and Node agree on last block, done downloading");
                }
            if (ki.getChainMan().currentHeight().get().compareTo(currentHeight) < 0) {
                ki.debug("Requesting blocks we're missing from the network");
                //pg.doneDownloading = true;
                if (ki.getOptions().lite) {
                    BlockRequest br = new BlockRequest();
                    br.lite = ki.getOptions().lite;
                    br.fromHeight = ki.getChainMan().currentHeight().get();
                    connMan.sendPacket(br);
                } else {
                    PackagedBlocksRequest pbr = new PackagedBlocksRequest();
                    pbr.fromBlock = ki.getChainMan().currentHeight().get();
                    connMan.sendPacket(pbr);
                }
            }
        }
    }

    @Override
    public void build(byte[] serialized) throws InvalidAmpBuildException {
        try {
            HeadlessPrefixedAmplet hpa = HeadlessPrefixedAmplet.create(serialized);
            ID = new String(hpa.getNextElement(), Charset.forName("UTF-8"));
            version = new String(hpa.getNextElement(), Charset.forName("UTF-8"));
            currentHeight = new BigInteger(hpa.getNextElement());
            mostRecentBlock = new String(hpa.getNextElement(), Charset.forName("UTF-8"));
            HeadlessAmplet ha = hpa.getNextElementAsHeadlessAmplet();
            chainVer = ha.getNextShort();
            isRelay = ha.getNextBoolean();
            startTime = ha.getNextLong();
            passive = ha.getNextBoolean();
        } catch (Exception e) {
            throw new InvalidAmpBuildException("Unable to create handshake from bytes");
        }
    }

    @Override
    public byte[] serializeToBytes() {
        HeadlessPrefixedAmplet hpa = HeadlessPrefixedAmplet.create();

        hpa.addElement(ID);
        hpa.addElement(version);
        hpa.addElement(currentHeight);
        hpa.addElement(mostRecentBlock);
        HeadlessAmplet ha = HeadlessAmplet.create();
        ha.addElement(chainVer);
        ha.addElement(isRelay);
        ha.addElement(startTime);
        ha.addElement(passive);
        hpa.addElement(ha);
        return hpa.serializeToBytes();
    }
}
