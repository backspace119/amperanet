package com.ampex.amperanet.packets;

import com.ampex.amperabase.IConnectionManager;
import com.ampex.amperabase.IKiAPI;
import com.ampex.amperabase.InvalidAmpBuildException;

public class DifficultyRequest implements Packet {

    @Override
    public void process(IKiAPI ki, IConnectionManager connMan, PacketGlobal pg) {
        DifficultyData dd = new DifficultyData();
        dd.difficulty = ki.getChainMan().getCurrentDifficulty().get();
        connMan.sendPacket(dd);
    }

    @Override
    public void build(byte[] serialized) throws InvalidAmpBuildException {

    }

    @Override
    public byte[] serializeToBytes() {
        return new byte[0];
    }
}
