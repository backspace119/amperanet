package com.ampex.amperanet.packets;

import com.ampex.amperabase.IConnectionManager;
import com.ampex.amperabase.IKiAPI;
import com.ampex.amperabase.InvalidAmpBuildException;

import java.math.BigInteger;

public class DifficultyData implements Packet {

    BigInteger difficulty;

    @Override
    public void process(IKiAPI ki, IConnectionManager connMan, PacketGlobal pg) {
        if (ki.getOptions().lite) {
             ki.getChainMan().setDifficulty(difficulty);
            ki.getNetMan().diffSet();
        }
    }

    @Override
    public void build(byte[] serialized) throws InvalidAmpBuildException {
        try {
            difficulty = new BigInteger(serialized);
        } catch (Exception e) {
            throw new InvalidAmpBuildException("Unable to create DifficultyData from bytes");
        }
    }

    @Override
    public byte[] serializeToBytes() {
        if (difficulty != null)
            return difficulty.toByteArray();
        else
            return new byte[0];
    }
}
