package com.ampex.amperanet.packets;

import amp.HeadlessAmplet;
import com.ampex.amperabase.IConnectionManager;
import com.ampex.amperabase.IKiAPI;
import com.ampex.amperabase.InvalidAmpBuildException;

public class Pong implements Packet {

    long latency;

    @Override
    public void process(IKiAPI ki, IConnectionManager connMan, PacketGlobal pg) {

        connMan.setCurrentLatency(System.currentTimeMillis() - latency);
        ki.getNetMan().setLive(true);

    }

    @Override
    public void build(byte[] serialized) throws InvalidAmpBuildException {
        try {
            HeadlessAmplet ha = HeadlessAmplet.create(serialized);
            latency = ha.getNextLong();
        } catch (Exception e) {
            throw new InvalidAmpBuildException("Unable to create Pong from bytes");
        }
    }

    @Override
    public byte[] serializeToBytes() {
        HeadlessAmplet ha = HeadlessAmplet.create();
        ha.addElement(latency);
        return ha.serializeToBytes();
    }
}
