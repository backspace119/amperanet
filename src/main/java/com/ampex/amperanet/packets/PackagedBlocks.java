package com.ampex.amperanet.packets;

import amp.Amplet;
import amp.ByteTools;
import amp.HeadlessPrefixedAmplet;
import com.ampex.amperabase.*;

import java.math.BigInteger;

public class PackagedBlocks implements Packet {


    private byte[] packagedBlocks;
    private BigInteger lastBlock;
    private long crc;

    @Override
    public void process(IKiAPI ki, IConnectionManager connMan, PacketGlobal pg) {
        if (lastBlock == null || packagedBlocks == null) return;
        if (!EncryptUtils.checkCRCValue(packagedBlocks, crc)) {

            return;
        }
        ki.debug("====================================Received PackagedBlocks. Size of byte array: " + packagedBlocks.length);
        HeadlessPrefixedAmplet hpa = HeadlessPrefixedAmplet.create(packagedBlocks);
        while (hpa.hasNextElement()) {
            IBlockAPI b = ki.getChainMan().formBlock(Amplet.create(hpa.getNextElement()));
            if (b == null) break;
            ki.downloadedTo(b.getHeight());
            ki.getStateManager().addBlock(b, connMan.getID());
        }

        if (lastBlock.compareTo(pg.startHeight) < 0) {
            //TODO cheap wait, need to set up object to lock on
            while(ki.getChainMan().currentHeight().get().compareTo(lastBlock.subtract(BigInteger.valueOf(10_000))) < 0) {}
            PackagedBlocksRequest pbr = new PackagedBlocksRequest();
            pbr.fromBlock = ki.getChainMan().currentHeight().get();
            connMan.sendPacket(pbr);
        } else {
            pg.doneDownloading = true;
        }
    }

    @Override
    public void build(byte[] serialized) throws InvalidAmpBuildException {
        try {
            HeadlessPrefixedAmplet hpa = HeadlessPrefixedAmplet.create(serialized);
            packagedBlocks = hpa.getNextElement();
            lastBlock = new BigInteger(hpa.getNextElement());
            byte[] crcArray = hpa.getNextElement();
            crc = ByteTools.buildLong(crcArray[0], crcArray[1], crcArray[2], crcArray[3], crcArray[4], crcArray[5], crcArray[6], crcArray[7]);
        } catch (Exception e) {
            throw new InvalidAmpBuildException("Unable to create PackagedBlocks from bytes");
        }

    }

    public static PackagedBlocks createPackage(IKiAPI ki, BigInteger startHeight) {
        PackagedBlocks pb = new PackagedBlocks();

        HeadlessPrefixedAmplet hpa = HeadlessPrefixedAmplet.create();
        int totalSize = 0;

        while (totalSize < 1_048_576_00) {
            startHeight = startHeight.add(BigInteger.ONE);
            byte[] block = ki.getChainMan().getByHeight(startHeight).serializeToAmplet().serializeToBytes();
            hpa.addBytes(block);
            totalSize += block.length;
            totalSize += 4;
            if (startHeight.compareTo(ki.getChainMan().currentHeight().get()) == 0) break;

        }
        pb.lastBlock = startHeight;
        pb.packagedBlocks = hpa.serializeToBytes();
        pb.crc = EncryptUtils.getCRCValue(pb.packagedBlocks);
        return pb;
    }

    @Override
    public byte[] serializeToBytes() {
        HeadlessPrefixedAmplet hpa = HeadlessPrefixedAmplet.create();
        hpa.ensureCapacity(1_048_676_00);
        hpa.addBytes(packagedBlocks);
        hpa.addElement(lastBlock);
        hpa.addElement(crc);
        return hpa.serializeToBytes();
    }
}
