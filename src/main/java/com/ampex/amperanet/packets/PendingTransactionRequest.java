package com.ampex.amperanet.packets;

import com.ampex.amperabase.IConnectionManager;
import com.ampex.amperabase.IKiAPI;
import com.ampex.amperabase.ITransAPI;
import com.ampex.amperabase.InvalidAmpBuildException;

public class PendingTransactionRequest implements Packet {

    @Override
    public void process(IKiAPI ki, IConnectionManager connMan, PacketGlobal pg) {
        for (ITransAPI trans : ki.getTransMan().getPending()) {
            TransactionPacket tp = new TransactionPacket();
            tp.trans = trans.serializeToAmplet().serializeToBytes();
            connMan.sendPacket(tp);
        }
    }

    @Override
    public void build(byte[] serialized) throws InvalidAmpBuildException {

    }

    @Override
    public byte[] serializeToBytes() {
        return new byte[0];
    }
}
