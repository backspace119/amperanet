package com.ampex.amperanet.packets;

import com.ampex.amperabase.IConnectionManager;
import com.ampex.amperabase.IKiAPI;
import com.ampex.amperabase.InvalidAmpBuildException;

public class DDFullChain implements Packet {

    @Override
    public void process(IKiAPI ki, IConnectionManager connMan, PacketGlobal pg) {
        PendingTransactionRequest ptr = new PendingTransactionRequest();
        connMan.sendPacket(ptr);
        if (ki.getOptions().poolRelay) {
            //ki.getPoolManager().updateCurrentHeight(ki.getChainMan().currentHeight());
            ki.doneDownloading();
        }
    }

    @Override
    public void build(byte[] serialized) throws InvalidAmpBuildException {

    }

    @Override
    public byte[] serializeToBytes() {
        return new byte[0];
    }
}
