package com.ampex.amperanet.packets;

import com.ampex.amperabase.*;

import java.nio.charset.Charset;
import java.util.List;

public class BlockEnd implements Packet {

    public String ID;

    @Override
    public void process(IKiAPI ki, IConnectionManager connMan, PacketGlobal pg) {
        if(ki.getOptions().pDebug)
        ki.debug("Received block end");
        BlockHeader bh = pg.headerMap.get(ID);
        List<ITransAPI> trans = pg.bMap.get(bh);
        IBlockAPI block = pg.formBlock(bh);
        if (block == null) {
            ki.debug("Something fucked up, block is null");
            return;
        }
        ki.debug("Block formed, adding transactions:");
        int i = 0;
        for (ITransAPI t : trans) {
            i++;
            ki.debug("Transaction " + i + " added");
            block.addTransaction(t);
        }
        ki.getStateManager().addBlock(block, connMan.getID());

        BlockAck ba = new BlockAck();
        ba.height = block.getHeight();
        ba.verified = true;
        connMan.sendPacket(ba);


    }

    @Override
    public void build(byte[] serialized) throws InvalidAmpBuildException {
        try {
            ID = new String(serialized, Charset.forName("UTF-8"));
        } catch (Exception e) {
            throw new InvalidAmpBuildException("Unable to create BLockEnd from bytes");
        }
    }

    @Override
    public byte[] serializeToBytes() {
        return ID.getBytes(Charset.forName("UTF-8"));
    }
}
