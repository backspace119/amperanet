package com.ampex.amperanet.packets;

import amp.ByteTools;
import amp.HeadlessPrefixedAmplet;
import com.ampex.amperabase.IConnectionManager;
import com.ampex.amperabase.IKiAPI;
import com.ampex.amperabase.ITransAPI;
import com.ampex.amperabase.InvalidAmpBuildException;

import java.math.BigInteger;

public class BlockAck implements Packet {

    public BigInteger height;
    public boolean verified;
    @Override
    public void process(IKiAPI ki, IConnectionManager connMan, PacketGlobal pg) {
        if (height == null) return;
        pg.cancelResend(height);
        if(verified)
        {
            if(ki.getChainMan().currentHeight().get().compareTo(height) > 0)
            {
                pg.sendBlock(height.add(BigInteger.ONE));
            }else if(ki.getChainMan().currentHeight().get().compareTo(height) == 0 && !pg.doneDownloading) {
                pg.doneDownloading = true;
                connMan.doneDownloading();
                connMan.sendPacket(new DoneDownloading());
                connMan.sendPacket(new DDFullChain());
                if(ki.getNetMan().isRelay())
                for(ITransAPI t:ki.getTransMan().getPending())
                {
                    TransactionPacket tp = new TransactionPacket();
                    tp.trans = t.serializeToAmplet().serializeToBytes();
                    connMan.sendPacket(tp);
                }
            } else if (ki.getChainMan().currentHeight().get().compareTo(height) == 0) {
                connMan.sendPacket(new DoneDownloading());
            }
        }
    }

    @Override
    public void build(byte[] serialized) throws InvalidAmpBuildException {
        try {
            HeadlessPrefixedAmplet hpa = HeadlessPrefixedAmplet.create(serialized);
            height = new BigInteger(hpa.getNextElement());
            verified = ByteTools.buildBoolean(hpa.getNextElement()[0]);
        } catch (Exception e) {
            throw new InvalidAmpBuildException("unable to build BlockAck packet from bytes");
        }
    }

    @Override
    public byte[] serializeToBytes() {
        HeadlessPrefixedAmplet hpa = HeadlessPrefixedAmplet.create();
        hpa.addElement(height);
        hpa.addElement(verified);
        return hpa.serializeToBytes();
    }
}
