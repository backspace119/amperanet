package com.ampex.amperanet.packets;

import com.ampex.amperabase.IConnectionManager;
import com.ampex.amperabase.IKiAPI;
import com.ampex.amperabase.InvalidAmpBuildException;

import java.math.BigInteger;

public class BlockSyncRequest implements Packet {

    public BigInteger height;

    @Override
    public void process(IKiAPI ki, IConnectionManager connMan, PacketGlobal pg) {
        if (height == null) return;
        if (height.compareTo(ki.getChainMan().currentHeight().get()) < 0) {
            pg.sendBlock(height.add(BigInteger.ONE));
        }
    }

    @Override
    public void build(byte[] serialized) throws InvalidAmpBuildException {
        try {
            height = new BigInteger(serialized);
        } catch (Exception e) {
            throw new InvalidAmpBuildException("Unable to create BlockSyncRequest from bytes");
        }
    }

    @Override
    public byte[] serializeToBytes() {
        if (height != null)
            return height.toByteArray();
        else return new byte[0];
    }
}
