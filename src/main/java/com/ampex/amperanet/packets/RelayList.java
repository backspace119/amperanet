package com.ampex.amperanet.packets;

import amp.HeadlessPrefixedAmplet;
import com.ampex.amperabase.IConnectionManager;
import com.ampex.amperabase.IKiAPI;
import com.ampex.amperabase.InvalidAmpBuildException;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

public class RelayList implements Packet {

    List<String> relays;

    @Override
    public void process(IKiAPI ki, IConnectionManager connMan, PacketGlobal pg) {

        if (pg.relays == null) pg.relays = new ArrayList<>();
        pg.relays.addAll(relays);
        ki.getNetMan().addRelays(relays);
        if (ki.getNetMan().getConnections().size() < 4) {
            for (String IP : ki.getNetMan().getRelays()) {
                boolean cont = false;
                for (IConnectionManager c : ki.getNetMan().getConnections()) {
                    if (c.getAddress().split(":")[0].replace("/", "").equals(IP)) {
                        cont = true;
                        break;
                    }
                }
                if (cont) continue;

                //TODO this was old code to attempt to connect to more relays, killing it for now
                //ki.getNetMan().attemptConnect(IP);
            }
        }

    }

    @Override
    public void build(byte[] serialized) throws InvalidAmpBuildException {
        try {
            HeadlessPrefixedAmplet hpa = HeadlessPrefixedAmplet.create(serialized);
            relays = new ArrayList<>();
            while (hpa.hasNextElement()) {
                relays.add(new String(hpa.getNextElement(), Charset.forName("UTF-8")));
            }
        } catch (Exception e) {
            throw new InvalidAmpBuildException("Unable to create RelayList from bytes");
        }
    }

    @Override
    public byte[] serializeToBytes() {

        HeadlessPrefixedAmplet hpa = HeadlessPrefixedAmplet.create();
        if (relays != null)
            for (String relay : relays) {
                hpa.addElement(relay);
            }
        return hpa.serializeToBytes();
    }
}
