package com.ampex.amperanet.packets;

import com.ampex.amperabase.IConnectionManager;
import com.ampex.amperabase.IKiAPI;
import com.ampex.amperabase.InvalidAmpBuildException;

public class UTXODataEnd implements Packet {

    @Override
    public void process(IKiAPI ki, IConnectionManager connMan, PacketGlobal pg) {

            //UTXODataStart.connIDs.remove(connMan.getID());

    }

    @Override
    public void build(byte[] serialized) throws InvalidAmpBuildException {

    }

    @Override
    public byte[] serializeToBytes() {
        return new byte[0];
    }
}
